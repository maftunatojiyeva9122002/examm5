import React, { useState } from "react";
import Navbar from "./component/Navbar";
import Carts from "./component/Carts";
import Cardss from "./component/cardss";

const App = () => {
  const [show, setShow] = useState(true);
  const [cart, setCart] = useState([]);

  const handleClick = (item) => {
    if (cart.indexOf(item) !== -1) return;
    setCart([...cart, item]);
  };

  const handleChange = (item, d) => {
    const ind = cart.indexOf(item);
    const arr = cart;
    arr[ind].amount += d;

    if (arr[ind].amount === 0) arr[ind].amount = 1;
    setCart([...arr]);
  };
  return (
    <>
      <Navbar setShow={setShow} size={cart.length} />
      {show ? (
        <Carts handleClick={handleClick} />
      ) : (
        <Cardss cart={cart} setCart={setCart} handleChange={handleChange} />
      )}
    </>
  );
};

export default App;
