import React from "react";
import Data from "../page/Data";
import Cart from "./Cart";
import "../styles/cardss.css";

const Carts = ({ handleClick }) => {
  return (
    <div className="container carts_box">
      {Data.map((item) => (
        <Cart key={item.id} item={item} handleClick={handleClick} />
      ))}
    </div>
  );
};

export default Carts;
