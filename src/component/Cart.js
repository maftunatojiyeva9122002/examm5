import React from "react";
import "../styles/cardss.css";

const Cart = ({ item, handleClick }) => {
  const { title, price, brand, images } = item;
  return (
    <>
      <div className="cards">
        <div className="image_box">
          <img src={images} alt={title} />
        </div>
        <div className="details">
          <p>{title}</p>
          <p>{brand}</p>
          <p>Price - {price}$</p>
          <button className="btn btn-primary" onClick={() => handleClick(item)}>
            Add to Cart
          </button>
        </div>
      </div>
    </>
  );
};

export default Cart;
